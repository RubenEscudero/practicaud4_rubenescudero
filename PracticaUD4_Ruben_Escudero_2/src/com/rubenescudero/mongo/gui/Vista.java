package com.rubenescudero.mongo.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.rubenescudero.mongo.base.Movil;
import com.rubenescudero.mongo.base.Ordenador;

import javax.swing.*;
import java.awt.*;

public class Vista {
    JTextField txtMarcaOrdenador;
    JTextField txtModeloOrdeandor;
    JTextField txtPrecioOrdenador;
    JButton buscarButton;
    JTextField txtBuscarOrdenador;
    JList listaOrdenadores;
    JButton altaOrdenadorButton;
    JButton modificarOrdenadorButton;
    JButton eliminarOrdenadorButton;
    DatePicker fechaOrdenador;
    JPanel panel;
    JTabbedPane tabbedPane1;
    JTextField txtMarcaMovil;
    JTextField txtModeloMovil;
    JTextField txtMemoria;
    JTextField txtUnidades;
    JTextField txtBuscarMovil;
    JButton listarButton;
    JButton altaMovilButton;
    JButton modificarMovilButton;
    JButton eliminarMovilButton;
    JList listMoviles;
    JFrame frame;

    DefaultListModel<Ordenador> dlmOrdenadores;
    DefaultListModel<Movil> dlmMoviles;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        addDLMs();
    }

    private void addDLMs(){
        dlmOrdenadores = new DefaultListModel<>();
        listaOrdenadores.setModel(dlmOrdenadores);
        dlmMoviles = new DefaultListModel<>();
        listMoviles.setModel(dlmMoviles);
    }
}
