package com.rubenescudero.mongo.gui;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.rubenescudero.mongo.base.Movil;
import com.rubenescudero.mongo.base.Ordenador;
import com.rubenescudero.mongo.util.Util;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Modelo {
    private final static String COLECCION_ORDENADORES = "Ordenadores";
    private final static String COLECCION_MOVILES = "Moviles";
    private final static String DATABASE = "TiendaPCs";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionOrdenadores;
    private MongoCollection coleccionMoviles;

    public void conectar(){
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionOrdenadores = baseDatos.getCollection(COLECCION_ORDENADORES);
        coleccionMoviles = baseDatos.getCollection(COLECCION_MOVILES);
    }

    public void desconctar(){
        client.close();
    }

    public void guardarOrdenador(Ordenador ordenador){
        coleccionOrdenadores.insertOne(ordenadorToDocument(ordenador));
    }

    public List<Ordenador> getOrdenadores(){
        ArrayList<Ordenador> lista = new ArrayList<>();
        Iterator<Document> iterator = coleccionOrdenadores.find().iterator();
        while(iterator.hasNext()){
            lista.add(documentToOrdenador(iterator.next()));
        }
        return lista;
    }

    public List<Ordenador> getOrdenadores(String text){
        ArrayList<Ordenador> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("marca", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("modelo", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> iterator = coleccionOrdenadores.find(query).iterator();
        while(iterator.hasNext()) {
            lista.add(documentToOrdenador(iterator.next()));
        }
        return lista;
    }

    public List<Ordenador> buscarOrdenador(String text){
        Document document = new Document("marca", text);
        FindIterable findIterable = coleccionOrdenadores.find(document);

        List<Ordenador> ordenadores = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()){
            ordenadores.add(documentToOrdenador(iterator.next()));
        }
        return ordenadores;
    }

    public Document ordenadorToDocument(Ordenador ordenador){
        Document document = new Document();
        document.append("marca", ordenador.getMarca());
        document.append("modelo", ordenador.getModelo());
        document.append("precio", ordenador.getPrecio());
        document.append("fechaLanzamiento", Util.formatearFecha(ordenador.getFechaDeLanzamiento()));
        return document;
    }

    public Ordenador documentToOrdenador(Document document){
        Ordenador ordenador = new Ordenador();
        ordenador.setId(document.getObjectId("_id"));
        ordenador.setModelo(document.getString("modelo"));
        ordenador.setMarca(document.getString("marca"));
        ordenador.setPrecio(document.getDouble("precio"));
        ordenador.setFechaDeLanzamiento(Util.parsearFecha(document.getString("fechaLanzamiento")));
        return ordenador;
    }

    public void modificarOrdenador(Ordenador ordenador){
        coleccionOrdenadores.replaceOne(new Document("_id", ordenador.getId()), ordenadorToDocument(ordenador));
    }

    public void borrarOrdenador(Ordenador ordenador){
        coleccionOrdenadores.deleteOne(ordenadorToDocument(ordenador));
    }

    public Document movilToDocument(Movil movil){
        Document document = new Document();
        document.append("marca", movil.getMarca());
        document.append("modelo", movil.getModelo());
        document.append("memoria", movil.getMemoria());
        document.append("unidades", movil.getUnidades());
        return document;
    }

    public Movil documentToMovil(Document document){
        Movil movil = new Movil();
        movil.setId(document.getObjectId("_id"));
        movil.setModelo(document.getString("modelo"));
        movil.setMarca(document.getString("marca"));
        movil.setMemoria(document.getInteger("memoria"));
        movil.setUnidades(document.getInteger("unidades"));
        return movil;
    }

    public void guardarMovil(Movil movil){
        coleccionMoviles.insertOne(movilToDocument(movil));
    }

    public List<Movil> getMovil(){
        ArrayList<Movil> lista = new ArrayList<>();
        Iterator<Document> iterator = coleccionMoviles.find().iterator();
        while(iterator.hasNext()){
            lista.add(documentToMovil(iterator.next()));
        }
        return lista;
    }

    public List<Movil> getMovil(String text){
        ArrayList<Movil> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("marca", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("modelo", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> iterator = coleccionOrdenadores.find(query).iterator();
        while(iterator.hasNext()) {
            lista.add(documentToMovil(iterator.next()));
        }
        return lista;
    }

    public List<Movil> buscarMovil(String text){
        Document document = new Document("marca", text);
        FindIterable findIterable = coleccionMoviles.find(document);

        List<Movil> movils = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()){
            movils.add(documentToMovil(iterator.next()));
        }
        return movils;
    }

    public void modificarMovil(Movil movil){
        coleccionMoviles.replaceOne(new Document("_id", movil.getId()), movilToDocument(movil));
    }

    public void borrarMovil(Movil movil){
        coleccionMoviles.deleteOne(movilToDocument(movil));
    }
}
