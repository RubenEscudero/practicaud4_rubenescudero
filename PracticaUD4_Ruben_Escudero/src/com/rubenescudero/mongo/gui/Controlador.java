package com.rubenescudero.mongo.gui;

import com.rubenescudero.mongo.base.Movil;
import com.rubenescudero.mongo.base.Ordenador;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, KeyListener {
    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        init();
    }

    private void init(){
        addActionListeners(this);
        addKeyListeners(this);
        addListListeners(this);
        modelo.conectar();
        listarOrdenadoree(modelo.getOrdenadores());
        listarMoviles(modelo.getMovil());
    }

    private void listarOrdenadoree(List<Ordenador> ordenadores) {
        vista.dlmOrdenadores.clear();
        for(Ordenador ordenador: ordenadores){
            vista.dlmOrdenadores.addElement(ordenador);
        }
    }

    private void listarMoviles(List<Movil> moviles) {
        vista.dlmMoviles.clear();
        for(Movil movil: moviles){
            vista.dlmMoviles.addElement(movil);
        }
    }

    private void addListListeners(Controlador controlador) {
        vista.listaOrdenadores.addListSelectionListener(controlador);
        vista.listMoviles.addListSelectionListener(controlador);
    }

    private void addKeyListeners(Controlador controlador) {
        vista.txtBuscarOrdenador.addKeyListener(controlador);
        vista.txtBuscarMovil.addKeyListener(controlador);
    }

    private void addActionListeners(Controlador controlador) {
        vista.altaOrdenadorButton.addActionListener(controlador);
        vista.eliminarOrdenadorButton.addActionListener(controlador);
        vista.modificarOrdenadorButton.addActionListener(controlador);
        vista.buscarButton.addActionListener(controlador);
        vista.altaMovilButton.addActionListener(controlador);
        vista.eliminarMovilButton.addActionListener(controlador);
        vista.modificarMovilButton.addActionListener(controlador);
        vista.listarButton.addActionListener(controlador);
    }

    private void cogerCamposOrdenador(Ordenador ordenador){
        ordenador.setMarca(vista.txtMarcaOrdenador.getText());
        ordenador.setModelo(vista.txtModeloOrdeandor.getText());
        ordenador.setPrecio(Double.parseDouble(vista.txtPrecioOrdenador.getText()));
        ordenador.setFechaDeLanzamiento(vista.fechaOrdenador.getDate());
    }

    private void cogerCamposMoviles(Movil movil){
        movil.setMarca(vista.txtMarcaMovil.getText());
        movil.setModelo(vista.txtModeloMovil.getText());
        movil.setMemoria(Integer.valueOf(vista.txtMemoria.getText()));
        movil.setUnidades(Integer.valueOf(vista.txtUnidades.getText()));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Ordenador ordenador;
        Movil movil;
        switch (e.getActionCommand()){
            case "Listar":
                listarOrdenadoree(modelo.getOrdenadores());
                break;
            case "Alta ordenador":
                ordenador = new Ordenador();
                cogerCamposOrdenador(ordenador);
                modelo.guardarOrdenador(ordenador);
                listarOrdenadoree(modelo.getOrdenadores());
                break;
            case "Modificar ordenador":
                ordenador = (Ordenador)vista.listaOrdenadores.getSelectedValue();
                cogerCamposOrdenador(ordenador);
                modelo.modificarOrdenador(ordenador);
                listarOrdenadoree(modelo.getOrdenadores());
                break;
            case "Eliminar ordenador":
                ordenador = (Ordenador)vista.listaOrdenadores.getSelectedValue();
                modelo.borrarOrdenador(ordenador);
                listarOrdenadoree(modelo.getOrdenadores());
                break;
            case "Listar Movil":
                listarMoviles(modelo.getMovil());
                break;
            case "Alta movil":
                movil = new Movil();
                cogerCamposMoviles(movil);
                modelo.guardarMovil(movil);
                listarMoviles(modelo.getMovil());
                System.out.println("Movil guardado");
                break;
            case "Modificar movil":
                movil = (Movil)vista.listMoviles.getSelectedValue();
                cogerCamposMoviles(movil);
                modelo.modificarMovil(movil);
                listarMoviles(modelo.getMovil());
                break;
            case "Eliminar movil":
                movil = (Movil)vista.listMoviles.getSelectedValue();
                modelo.borrarMovil(movil);
                listarMoviles(modelo.getMovil());
                break;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        try {
            Ordenador ordenador = (Ordenador) vista.listaOrdenadores.getSelectedValue();
            vista.txtMarcaOrdenador.setText(ordenador.getMarca());
            vista.txtModeloOrdeandor.setText(ordenador.getModelo());
            vista.txtPrecioOrdenador.setText(String.valueOf(ordenador.getPrecio()));
            vista.fechaOrdenador.setDate(ordenador.getFechaDeLanzamiento());
            Movil movil = (Movil) vista.listMoviles.getSelectedValue();
            vista.txtMarcaMovil.setText(movil.getMarca());
            vista.txtModeloMovil.setText(movil.getModelo());
            vista.txtMemoria.setText(String.valueOf(movil.getMemoria()));
            vista.txtUnidades.setText(String.valueOf(movil.getUnidades()));
        }
        catch (NullPointerException e1){
            System.out.println("Otra lista cogida");
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscarOrdenador){
            listarOrdenadoree(modelo.buscarOrdenador(vista.txtBuscarOrdenador.getText()));
        }
        if(e.getSource() == vista.txtBuscarMovil){
            listarMoviles(modelo.buscarMovil(vista.txtBuscarMovil.getText()));
        }
    }
}
