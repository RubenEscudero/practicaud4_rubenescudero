package com.rubenescudero.mongo;

import com.rubenescudero.mongo.gui.Controlador;
import com.rubenescudero.mongo.gui.Modelo;
import com.rubenescudero.mongo.gui.Vista;

public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
